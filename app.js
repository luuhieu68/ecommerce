const path = require('path')
const express = require('express')
const bodyParser = require('body-parser')
const methodOverride = require('method-override')
const hbs = require('express-handlebars')
const dotevn = require('dotenv')
const cookieParser = require('cookie-parser')
const session = require('express-session')
const paginate = require('express-paginate')

// 

const db = require('./src/configs/db.config')
const route = require('./src/routes/index.route')
// let Cart = require('./src/controllers/dashboard/cart.controller')
let Cart = require('./src/models/cart.model')

// const cartRouter = require('./src/routes/dashboard/cart.route')


// 
const app = express()
// app.use(paginate.middleware(10, 50))
//setup bodyParser
app.use(bodyParser.urlencoded({ extended: true }))
app.use(methodOverride('_method'))
// cookieParser
app.use(cookieParser())
// session
app.use(
  session({
    secret: 'notsecret',
    saveUninitialized: true,
    resave: false,
    cookie: { httpOnly: true, maxAge: null },
  })
)
// app.use(session({
//     cookie: {httpOnly: true, maxAge: 30*24*60*60*1000},
//     secret: 'hihi',
//     resave: false,
//     saveUninitialized: true
// }))
// // use cartController
// app.use((req, res, next) =>{
//     var cart = new Cart(req.session.cart ? req.session.cart:{})
//     req.session.cart = cart
//     res.locals.totalQuantity = cart.totalQuantity
//     next()
// })

// res.locals is an object passed to hbs engine
app.use((req, res, next) => {
  var cart = new Cart(req.session.cart ? req.session.cart : {})
  req.session.cart = cart
  res.locals.totalQuantity = cart.totalQuantity
  res.locals.fullname = req.session.user ? req.session.user.fullname : ''
  res.locals.isLogin = req.session.user ? true : false
  next()
})

//setup handlebars
app.engine('.hbs', hbs({
  extname: '.hbs',
  helpers: {
    showOrHide: (a, b, opts) => {
      if (a !== b) {
        return opts.fn(this);
      } else {
        return opts.inverse(this);
      }
    },
    show: (a, b) => {
      if (a !== b) {
        return true
      } else {
        return false
      }
    }
  },
  runtimeOptions: {
    allowProtoPropertiesByDefault: true,
    allowProtoMethodsByDefault: true,
  }
}))


app.set('views', path.join(__dirname, 'src/views'))
app.set('view engine', 'hbs')
//public file
app.use(express.static(path.join(__dirname, 'src/public')))


// 

dotevn.config()
db.connectDatabase()
route(app)
const port = process.env.PORT || 2000
app.listen(port, () => {
  console.log(`no la: ${port} `)

})

