$(document).ready(() =>{
    $('.add-to-cart').on('click', addToCart)
})
function addToCart(){
    var id = $(this).data('id')
    var quantity = $('#quantity') ? $('#quantity').val() : 1
    $.ajax({
        url: "/add-to-cart",
        type: 'POST',
        data: {id, quantity},
        success: function(ressult){
            $('#cart-badge').load(" #cart-badge > *")
        }
    })
}

function updateCart(id, quantity){
    if(quantity == 0){
        removeCartItem(id)
    } else{
        updateCartItem(id, quantity)
    }
}

function updateCartItem(id, quantity){
    $.ajax({
        url: '/cart',
        type: 'PUT',
        data: {id, quantity},
        success: function(result){
            $('#cart-badge').load(" #cart-badge > *")
            $(`#${id}`).html(result.item.price)
            $('#sub-total').load(" #sub-total > *")
            $('#sub-price').html(result.totalPrice)
            $('#total-price').html(result.totalPrice)
        }
    })
}

function removeCartItem(id){
    $.ajax({
        url: '/cart',
        type: 'DELETE',
        data: {id},
        success: function(result){
            $('#cart-badge').load(" #cart-badge > *")
            $('#sub-price').html(result.totalPrice)
            $('#total-price').html(result.totalPrice)
            $(`#item${id}`).remove()
        }
    })
}
function clearCart(){
    $.ajax({
        url: '/cart/all',
        type: 'DELETE',
        success: function(){
            $('#cart-badge').load(" #cart-badge > *")
            
        }
    })
}

// $(".add-to-cart").click(function(e) {
//     const id = $(this).attr("data-id");
//     $.ajax({
//       url: "/add-to-cart/" + id,
//       method: "POST",
//       success: data => {
//         $("#cart-badge").load(" #cart-badge > *")
//       }
//     })
//   })