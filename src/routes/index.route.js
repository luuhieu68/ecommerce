const productRouter = require('./dashboard/product.route')
const fakeRouter = require('./fake.route')
const cartRouter = require('./dashboard/cart.route')
const usertRouter = require('./user.route')
const indexCon = require('../controllers/index.controller')



function route(app) {
  app.get('/', indexCon.getAll)
  app.use(cartRouter)
  app.use('/dashboard', productRouter)
  app.use('/user', usertRouter)
  app.use('/hihi', fakeRouter)
  // app.get('/add-to-cart/:productId',cartRouter)


}
module.exports = route