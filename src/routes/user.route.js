const express = require('express')
const router = express.Router()
const userCon = require('../controllers/user.controller')

// Xu ly login
router.get('/login', (req, res) => {
    if (req.session.user) {
        res.redirect('/')
    } else {
        res.render('auth/login')
    }
})
router.post('/login', (req, res, next) => {
    let email = req.body.username
    let password = req.body.password
    let keepLogin = (req.body.keepLogin != undefined)
    userCon
        .getUserByEmail(email)
        .then(user => {
            if (user) {
                if (userCon.comparePassword(password, user.password)) {
                    req.session.cookie.maxAge = keepLogin ? 30 * 24 * 60 * 60 * 1000 : null
                    req.session.user = user
                    res.redirect('/')
                } else {
                    res.render('auth/login', {
                        message: 'Mật khẩu không chính xác',
                        type: 'alert-danger'
                    })
                }

            } else {
                res.render('auth/login', {
                    message: 'Tài khoản chưa được đăng ký',
                    type: 'alert-danger'
                })
            }

        })
})

// Xu ly register
router.get('/register', (req, res) => {
    if (req.session.user) {
        res.redirect('/')
    } else {
        res.render('auth/register')

    }

})
router.post('/register', (req, res) => {


    let username = req.body.email
    let fullname = req.body.name
    let password = req.body.password
    // Kiểm tra user ton tai chua
    userCon
        .getUserByEmail(username)
        .then(user => {
            if (user) {
                return res.render('auth/register', {
                    message: `Email ${username} đã được sử dụng. Vui lòng nhập email khác`,
                    type: 'alert-danger'
                })
            }
            // Khoi tao tai khoan
            user = {
                fullname,
                username,
                password
            }
            return userCon
                .createUser(user)
                .then(user => {
                        res.render('auth/login', {
                            message: 'Đăng ký thành công!',
                            type: 'alert-primary'
                        })
                })
        })
})

router.get('/register', function (req, res) {
    res.render('auth/register', {
        success: req.session.success,
        errors: req.session.errors
    });
    req.session.errors = null;
});

// Xu ly Logout
router.get('/logout', (req, res, next) => {
    req.session.destroy(error => {
        if (error) {
            return next(error)
        }
        return res.redirect('login')
    })
})

module.exports = router