const express = require('express')
const router = express.Router()
const cartCon = require('../../controllers/dashboard/cart.controller')
const Products = require('../../models/product.model')

router.get('/cart', cartCon.getCart)
router.post('/add-to-cart', cartCon.addCart)
router.put('/cart', cartCon.updateCart)
router.delete('/cart', cartCon.deleteCart)
router.delete('/cart', cartCon.deleteAll)
// router.get('/cart', (req,res) =>{
//     var cart = req.session.cart
//     res.locals.cart = cart.getCart()
//     res.render('dashboard/cart/index')
//     // res.json(cart)
// })
// router.post('/add-to-cart', (req,res, next) =>{
//     var productId = req.body.id;
//         var quantity = isNaN(req.body.quantity) ? 1 : req.body.quantity
//         Products
//         .findById(productId)
//         .then(product =>{
//             var cartItem = req.session.cart.add(product, productId, quantity)
//             res.json(cartItem)
//         })
//         .catch(error => next(error))
// })

module.exports = router