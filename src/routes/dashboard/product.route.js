const { query } = require('express')
const express = require('express')
const router = express.Router()
const productCon = require('../../controllers/dashboard/product.controller')


// router.get('/', (req, res, next) =>{
//     productCon
//     .getAll(req.query)
//     .then(data =>{
//         res.locals.products = data
//         res.render('dashboard/shop/index')
//     })
// })
// router.post("/:productType*?", productCon.getLimit);
router.get('/', productCon.getAll)
router.get('/category/:name', productCon.getByCategory)
router.get('/products/:id', productCon.getById)
// router.get('/product:page', ProductCon.getAll)




module.exports = router