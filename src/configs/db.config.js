const mongoose = require("mongoose")

async function connectDatabase() {
    const mongoDbUrl = `mongodb://${process.env.MONGO_HOST}:${process.env.MONGO_PORT}/${process.env.MONGO_DB}`
    console.log(`connect to ${mongoDbUrl}`)
    mongoose.Promise = global.Promise

    mongoose
    .connect(mongoDbUrl, {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
        useFindAndModify: false
    })
    .then(() =>{
        console.log('Ket noi thanh cong')
    })
    .catch((err) =>{
        console.log('Ket noi khong thanh cong')
        process.exit()
    })
}
module.exports = { connectDatabase }