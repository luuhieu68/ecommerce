const mongoose = require("mongoose")
var Schema = mongoose.Schema
const mongoosePaginate = require('mongoose-paginate-v2');

var Product = new Schema({
    name: String,
    price: Number,
    brand: String,
    label: String,
    cover: Object,
    category_id: { type: Schema.Types.ObjectId, ref: 'Category' },
},{
    timestamps: true
})
Product.plugin(mongoosePaginate);

module.exports = mongoose.model('Product', Product)
