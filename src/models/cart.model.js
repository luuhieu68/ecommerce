// module.exports = function Cart(oldCart) {
//   this.items = oldCart.items || {};
//   this.totalQty = oldCart.totalQty || 0;
//   this.totalPrice = oldCart.totalPrice || 0;
//   this.numItems = oldCart.numItems || 0;

//   this.add = (item, id, qty) => {
//     const itemQty = qty ? Number(qty) : 1;
//     var storeItem = this.items[id];
//     if (!storeItem) {
//       storeItem = this.items[id] = { item: item, qty: 0, price: 0, images: '' };
//       this.numItems++;
//     }
//     storeItem.qty += itemQty;
//     storeItem.price = storeItem.item.price * storeItem.qty;
//     // storeItem.images = storeItem.item.images[0];
//     this.totalQty += itemQty;
//     this.totalPrice += storeItem.item.price;
//   };

//   this.changeQty = (item, id, qty) => {
//     const itemQty = qty ? Number(qty) : 1;
//     var storeItem = this.items[id];
//     if (!storeItem) {
//       storeItem = this.items[id] = { item: item, qty: 0, price: 0, images: '' };
//       this.numItems++;
//     }
//     let oldQty = storeItem.qty;
//     storeItem.qty = itemQty;
//     storeItem.price = storeItem.item.price * storeItem.qty;
//     storeItem.images = storeItem.item.images[0];
//     this.totalQty += itemQty - oldQty;
//     this.totalPrice += storeItem.price - storeItem.item.price * oldQty;
//   };

//   this.deleteItem = id => {
//     var storeItem = this.items[id];
//     this.totalQty -= storeItem.qty;
//     this.totalPrice -= storeItem.price;
//     this.numItems--;
//     delete this.items[id];
//   };

//   this.addCart = cart => {
//     for (var id in cart.items) {
//       var storeItem = this.items[id];

//       if (!storeItem) {
//         storeItem = this.items[id] = {
//           item: cart.items[id].item,
//           qty: cart.items[id].qty,
//           price: cart.items[id].price,
//           images: cart.items[id].images
//         };

//         this.numItems++;
//       } else {
//         storeItem.qty += parseInt(cart.items[id].qty);
//         storeItem.price += cart.items[id].price;
//       }
//       this.totalQty += cart.items[id].qty;
//       this.totalPrice += cart.items[id].price;
//     }
//     return this;
//   };

//   this.generateArray = () => {
//     var arr = [];
//     for (var id in this.items) {
//       arr.push(this.items[id]);
//     }
//     return arr;
//   };
// };

module.exports = function Cart(oldCart) {
    this.items = oldCart.items || {};
    this.totalQuantity = oldCart.totalQuantity || 0;
    this.totalPrice = oldCart.totalPrice || 0;
    this.address = oldCart.address || {};
    this.paymentMethod = oldCart.paymentMethod || "COD";

    this.getTotalQuantity = () => {
        var quantity = 0;
        for (var id in this.items) {
            quantity += parseInt(this.items[id].quantity);
        }
        return quantity;
    };

    this.getTotalPrice = () => {
        var price = 0;
        for (var id in this.items) {
            price += parseFloat(this.items[id].price);
        }
        price = parseFloat(price);
        return price;
    };

    this.add = (item, id, quantity) => {
        var storedItem = this.items[id];
        if (!storedItem) {
            this.items[id] = { item: item, quantity: 0, price: 0 };
            storedItem = this.items[id];
        }
        storedItem.item.price = parseFloat(storedItem.item.price);
        storedItem.quantity += parseInt(quantity);
        storedItem.price = parseFloat(storedItem.item.price * storedItem.quantity);
        this.totalQuantity = this.getTotalQuantity();
        this.totalPrice = this.getTotalPrice();
        return this.getCartItem(id);
    };

    this.remove = (id) => {
        var storedItem = this.items[id];
        if (storedItem) {
            delete this.items[id];
            this.totalQuantity = this.getTotalQuantity();
            this.totalPrice = this.getTotalPrice();
        }
    };

    this.update = (id, quantity) => {
        var storedItem = this.items[id];
        if (storedItem && quantity >= 1) {
            storedItem.quantity = quantity;
            storedItem.price = parseFloat(storedItem.item.price * storedItem.quantity);
            this.totalQuantity = this.getTotalQuantity();
            this.totalPrice = this.getTotalPrice();
        }
        return this.getCartItem(id);
    };

    this.empty = () => {
        this.items = {};
        this.totalQuantity = 0;
        this.totalPrice = 0;
    };

    this.generateArray = () => {
        var arr = [];
        for (var id in this.items) {
            this.items[id].item.price = parseFloat(this.items[id].item.price).toFixed(2);
            this.items[id].price = parseFloat(this.items[id].price).toFixed(2);
            arr.push(this.items[id]);
        }
        return arr;
    };

    this.getCart = function() {
        var cart = {
            items: this.generateArray(),
            totalQuantity: this.totalQuantity,
            totalPrice: this.totalPrice,
            address: this.address,
            paymentMethod: this.paymentMethod
        };
        return cart;
    }

    this.getCartItem = function(id) {
        var cartItem = {
            item: this.items[id],
            totalQuantity: this.totalQuantity,
            totalPrice: this.totalPrice,
        }
        return cartItem;
    }
};