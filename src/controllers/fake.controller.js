const Product = require('../models/product.model')
const faker = require('faker');


class FakeCon {
    fake(req, res) {
        for(let i = 0; i < 96; i++) {
            const newprd = new Product();
            newprd.name = faker.commerce.productName()
            newprd.price = faker.commerce.price()
            newprd.cover = faker.image.image()
            
            newprd.save((err)=>{
                if (err) { return next(err); }
              });
            }
            res.redirect('/');   
    }

}
module.exports = new FakeCon()

