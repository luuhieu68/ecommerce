var bcrypt = require('bcryptjs');
const User = require('../models/user.model')

class userCon {
    // login(req, res){
    //     res.render('auth/login')
    // }
    // register(req, res){
    //     res.render('auth/register')
    // }
    // registerUser(req, res){
    //     let user = {
    //         username: req.body.email,
    //         fullname: req.body.name,
    //         password: req.body.password
    //     }
    //     let confirmPassword = req.body.confirmPassword
    //     let keepLogin = (req.body.keepLogin != undefined)
    //     console.log(user)
    // }
    getUserByEmail = (email) =>{
        return User.findOne({username: email})
    }
    createUser = (user) =>{
        var salt = bcrypt.genSaltSync(10)
        user.password = bcrypt.hashSync(user.password, salt)
        return User.create(user)
    }
    comparePassword = (password, hash) =>{
        return bcrypt.compareSync(password, hash)
    }

}
module.exports = new userCon()

