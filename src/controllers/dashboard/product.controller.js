const { hasNextPages } = require('express-paginate');
const Product = require('../../models/product.model')
const Category = require('../../models/category.model')

var sort_value
var SORT_ITEM;
var ptype;
var ptypesub;
var pprice = 999999;
var psize;
var plabel;
var plowerprice;
var price;
var orderby;
var searchText;


class productCon {
    // getAll(req, res) {
    //     Product.find({})
    //     .then(data =>{
    //         res.json(data)
    //     })
    //     .catch(err =>{
    //         res.json(err)
    //     })
    // }
    // getAll(req, res) {
    //     var page = parseInt(req.query.page) || 0; //for next page pass 1 here
    //     var limit = parseInt(req.query.limit) || 3;
    //     var query = {};
    //     Product.find(query)
    //         .sort({ updatedAt: -1 })
    //         .skip(page * limit) //Notice here
    //         .limit(limit)
    //         .exec((err, products) => {
    //             if (err) {
    //                 return res.json(err);
    //             }
    //             Product.countDocuments(query).exec((count_error, count) => {
    //                 if (err) {
    //                     return res.json(count_error);
    //                 }
    //                 return res.json({
    //                     total: count,
    //                     page: page,
    //                     pageSize: products.length,
    //                     products: products
    //                   })
    //                 // res.render('dashboard/shop/index', {products: products,total: count,page: page,pageSize: products.length})

    //             });
    //         });
    // }
    // getByCategory
    getByCategory(req, res) {
        let name
        var ITEM_PER_PAGE = parseInt(req.query.limit) || 9
        orderby = { updatedAt: -1 }
        var minPrice = req.query.min || 0
        var maxPrice = req.query.max || 1000000
        var page = +parseInt(req.query.page) || 1
        let totalProducts
        if (req.query.orderby) {
            switch (req.query.orderby) {
                case 'priceAsc':
                    sort_value = "Giá tăng dần"
                    orderby = { price: 1 }
                    break;
                case 'priceDesc':
                    sort_value = "Giá giảm dần"
                    orderby = { price: -1 }
                    break;
                case 'updateAsc':
                    sort_value = "Hàng mới"
                    orderby = { updatedAt: -1 }
                    break;
                default:
                    sort_value = "Hàng mới"
                    orderby = { updatedAt: -1 }
            }

        }
        switch (req.params.name) {
            case 'quan':
                name = "quần"
                break;
            case 'ao':
                name = "áo"
                break;

            default:
                name = ""
        }

        Product.find({category: new RegExp(name, "i")})
            .countDocuments()
            .then(numProducts => {
                totalProducts = numProducts
                return Product.find({
                    price: { $gt: minPrice, $lt: maxPrice },
                    category: new RegExp(name, "i")
                })
                    .skip((page - 1) * ITEM_PER_PAGE)
                    .limit(ITEM_PER_PAGE)
                    .sort(orderby)
            })

            .then(products => {
                res.render('dashboard/shop/index', {
                    products: products,
                    sort_value: sort_value,
                    currentPage: page,
                    ITEM_PER_PAGE: ITEM_PER_PAGE,
                    totalProducts: totalProducts,
                    hasNextPage: ITEM_PER_PAGE * page < totalProducts,
                    hasPreviousPage: page > 1,
                    nextPage: page + 1,
                    previousPage: page - 1,
                    lastPage: Math.ceil(totalProducts / ITEM_PER_PAGE),
                })
            })
            .catch(err => {
                res.json(err)
            })


    }


    getAll(req, res) {
        var ITEM_PER_PAGE = parseInt(req.query.limit) || 9
        orderby = { updatedAt: -1 }
        var minPrice = req.query.min || 0
        var maxPrice = req.query.max || 1000000
        var page = +parseInt(req.query.page) || 1
        let totalProducts
        if (req.query.orderby) {
            switch (req.query.orderby) {
                case 'priceAsc':
                    sort_value = "Giá tăng dần"
                    orderby = { price: 1 }
                    break;
                case 'priceDesc':
                    sort_value = "Giá giảm dần"
                    orderby = { price: -1 }
                    break;
                case 'updateAsc':
                    sort_value = "Hàng mới"
                    orderby = { updatedAt: -1 }
                    break;
                default:
                    sort_value = "Hàng mới"
                    orderby = { updatedAt: -1 }
            }

        }

        Product.find({})
            .countDocuments()
            .then(numProducts => {
                totalProducts = numProducts
                return Product.find({
                    price: { $gt: minPrice, $lt: maxPrice }
                })
                    .skip((page - 1) * ITEM_PER_PAGE)
                    .limit(ITEM_PER_PAGE)
                    .sort(orderby)
            })

            .then(products => {
                res.render('dashboard/shop/index', {
                    products: products,
                    sort_value: sort_value,
                    currentPage: page,
                    ITEM_PER_PAGE: ITEM_PER_PAGE,
                    totalProducts: totalProducts,
                    hasNextPage: ITEM_PER_PAGE * page < totalProducts,
                    hasPreviousPage: page > 1,
                    nextPage: page + 1,
                    previousPage: page - 1,
                    lastPage: Math.ceil(totalProducts / ITEM_PER_PAGE),
                })
            })
            .catch(err => {
                res.json(err)
            })
    }

    // getAll = (query) => {
    //     return new Promise((resolve, reject) => {

    //         if (query.orderby) {
    //             switch (query.orderby) {
    //                 case 'nameAsc':
    //                     sort_value = "1"
    //                     orderby = { name: 1 }
    //                     break;
    //                 case 'nameDesc':
    //                     sort_value = "2"
    //                     orderby = { name: -1 }
    //                     break;
    //                 case 'priceAsc':
    //                     sort_value = "3"
    //                     orderby = { price: 1 }
    //                     break;
    //                 case 'priceDesc':
    //                     sort_value = "4"
    //                     orderby = { price: -1 }
    //                     break;
    //                 case 'updateAsc':
    //                     sort_value = "5"

    //                     orderby = { updatedAt: 1 }
    //                     break;
    //                 case 'updateDesc':

    //                     orderby = { updatedAt: -1 }
    //                     break;
    //                 default:
    //                     orderby = { updatedAt: -1 }
    //             }

    //         }
    //         orderby = { updatedAt: -1 }
    //         console.log(sort_value)

    //         Product
    //             .find({})
    //             .sort(orderby)
    //             .limit(ITEM_PER_PAGE)
    //             .then(data => resolve(data))
    //             .catch(error => reject(new Error(error)))
    //     })
    // }

    // getLimit(req, res) {
    //     ITEM_PER_PAGE = parseInt(req.body.limit)
    //     console.log(ITEM_PER_PAGE)
    //     res.redirect("back")
    // }

    getPaging(req, res) {
        Product.paginate({}, { offset: 20, limit: 10 })
            .then(products => {
                res.json(products)
            })
            .catch(err => {
                res.json(err)
            })
    }
    // getById
    getById(req, res, next) {
        Product.findById(req.params.id)
            .then(product => {
                res.render('dashboard/product/index', { product })
            })
            .catch(err => {
                res.json(err)
            })
    }
    // getByCategory
    // getByCategory(req, res) {
    // Product.find({})
    // .populate('category_id')
    // .exec()
    // .then(products => {
    //     res.json(products)
    //     console.log(products)
    // })
    // .catch(err =>{
    //     console.log(err)
    // })
    //     Product.find({})
    //     .then(data =>{
    //         res.json(data)
    //     })
    //     .catch(err =>{
    //         res.json(err)
    //     })
    // }

}
module.exports = new productCon()

