const { populate } = require('../models/product.model')
const Products = require('../models/product.model')


class indexCon {
    getAll(req, res, next) {
        Products
            .find({})
            .limit(8)
            .sort({ updatedAt: -1 })
            .then(products => {
                Products
                    .find({ label: 'bestsale' })
                    .limit(6)
                    .sort({ updatedAt: -1 })
                    .then(productsBestSale => {
                        Products
                            .find({ label: 'onsale' })
                            .limit(9)
                            .sort({ updatedAt: -1 })
                            .then(productsSale => {
                                res.render('index', { products, productsBestSale, productsSale })
                            })
                    })

            })

            .catch(next)
    }
}
module.exports = new indexCon()

